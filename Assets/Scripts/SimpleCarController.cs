﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WheelInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor;
    public bool steering;
}

public class SimpleCarController : MonoBehaviour
{

    public List<WheelInfo> wheelInfos;
    public float maxMotorTorque;
    public float maxSteeringAngle;

    void FixedUpdate()
    {
        float motor = maxMotorTorque * Input.GetAxis("Vertical");
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

        foreach (var wheelInfo in wheelInfos)
        {
            if (wheelInfo.steering)
            {
                wheelInfo.leftWheel.steerAngle = steering;
                wheelInfo.rightWheel.steerAngle = steering;
            }

            if (wheelInfo.motor)
            {
                wheelInfo.leftWheel.motorTorque = motor;
                wheelInfo.rightWheel.motorTorque = motor;
            }

            ApplyLocalPositionToWheels(wheelInfo.leftWheel);
            ApplyLocalPositionToWheels(wheelInfo.rightWheel);
        }
    }

    private void ApplyLocalPositionToWheels(WheelCollider wheelCollider)
    {
        if (wheelCollider.transform.childCount == 0)
        {
            Debug.LogError("Faltan los modelos de ruedas como hijos directos del wheelCollider...");
            return;
        }

        Transform visualWheel = wheelCollider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;

        wheelCollider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

}
