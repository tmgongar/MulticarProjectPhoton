﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class NetworkPlayer : MonoBehaviourPun
{
    public GameObject localCam;
    public GameObject localCMV;

    public Material material1, material2;

    Material[] carMaterial;

    Behaviour[] componentsToDisable;

    private void Start()
    {
        GameObject child = transform.GetChild(0).gameObject;

        carMaterial = child.GetComponent<MeshRenderer>().materials;

        Debug.Log(carMaterial[3].name);

        if (!photonView.IsMine)
        {
            localCam.SetActive(false);
            localCMV.SetActive(false);
            carMaterial[3].color = new Color(0,1,0);
            Debug.Log(carMaterial[3].color);

            // Desactivamos todos los scripts relacionados con otros players, excepto éste
            MonoBehaviour[] scripts = GetComponents<MonoBehaviour>();            

            for (int i = 0; i < scripts.Length; i++)
            {
                if (scripts[i] is NetworkPlayer) continue;
                else if (scripts[i] is PhotonView) continue;

                scripts[i].enabled = false;
            }

            //foreach (var components in componentsToDisable)
            //{
            //    components.enabled = false;
            //}
        }

        if (photonView.IsMine) carMaterial[3].color = new Color(1, 0, 0);
        Debug.Log(carMaterial[3].color);
    }
}
